package com.epam.clientserver;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageParserTest {

    @Test
    void getReceiverLogin() {
        MessageParser parser = MessageParser.parse("yura: @user, hello");
        Assert.assertEquals("user", parser.getReceiverLogin());
        parser = MessageParser.parse("yura: hello, world");
        Assert.assertEquals("", parser.getReceiverLogin());
    }

    @Test
    void getMessageFrom() {
        MessageParser parser = MessageParser.parse("yura: @user, hello");
        Assert.assertEquals("hello", parser.getMessage());
        parser = MessageParser.parse("yura: hello, world");
        Assert.assertEquals("hello, world", parser.getMessage());
    }
}