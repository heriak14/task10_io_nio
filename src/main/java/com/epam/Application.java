package com.epam;

import com.epam.view.View;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException, InterruptedException {
        new View().show();
    }
}
