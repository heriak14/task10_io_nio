package com.epam.clientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

public class Server {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final int BUFFER_SIZE = 1024;
    private static final int PORT = 9999;
    private Selector selector;
    private ServerSocketChannel server;
    private Map<String, SocketChannel> clients;

    private Server() {
        try {
            initialize();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    private void initialize() throws IOException {
        selector = Selector.open();
        server = ServerSocketChannel.open();
        ServerSocket serverSocket = server.socket();
        InetSocketAddress address = new InetSocketAddress(InetAddress.getLocalHost(), PORT);
        serverSocket.bind(address);
        server.configureBlocking(false);
        int ops = server.validOps();
        server.register(selector, ops, null);
        clients = new HashMap<>();
    }

    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.start();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    private void start() throws IOException {
        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> i = selectedKeys.iterator();
            while (i.hasNext()) {
                SelectionKey key = i.next();
                if (key.isAcceptable()) {
                    registerClient(server.accept());
                } else if (key.isReadable()) {
                    handleMessage(key);
                }
                i.remove();
            }
            updateClients();
        }
    }

    private void registerClient(SocketChannel client) throws IOException {
        ByteBuffer clientLogin = ByteBuffer.allocate(BUFFER_SIZE);
        client.read(clientLogin);
        String login = new String(clientLogin.array()).trim();
        LOGGER.info(login + " was successfully connected...\n");
        clients.put(login, client);
        client.configureBlocking(false);
        client.register(selector, SelectionKey.OP_READ);
    }

    private static void sendMessage(SocketChannel client, String message) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
        buffer.rewind();
        client.write(buffer);
    }

    private String readMessage(SelectionKey key) throws IOException {
        SocketChannel client = (SocketChannel) key.channel();
        //sendClientList(client);
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        client.read(buffer);
        return new String(buffer.array()).trim();
    }

    private void handleMessage(SelectionKey key) throws IOException {
        String input = readMessage(key);
        if (input.isEmpty()) {
            return;
        }
        MessageParser parser = MessageParser.parse(input);
        List<SocketChannel> receivers = getReceiver(parser.getReceiverLogin());
        receivers.forEach(c -> {
            try {
                sendMessage(c, parser.getSenderLogin() + ": " + parser.getMessage());
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        });
    }

    private void sendClientList(SocketChannel client) throws IOException {
        ByteBuffer clientsBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        clientsBuffer.put(String.join("\n", clients.keySet()).getBytes());
        clientsBuffer.flip();
        client.write(clientsBuffer);
    }

    private List<SocketChannel> getReceiver(String receiverLogin) {
        System.out.println(receiverLogin);
        List<SocketChannel> receivers = new ArrayList<>();
        if (clients.containsKey(receiverLogin)) {
            receivers.add(clients.get(receiverLogin));
            return receivers;
        } else {
            return new ArrayList<>(clients.values());
        }
    }

    private void updateClients() {
        clients.entrySet().removeIf(e -> {
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
            try {
                return e.getValue().read(buffer) == -1;
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage());
                return false;
            }
        });
    }
}
