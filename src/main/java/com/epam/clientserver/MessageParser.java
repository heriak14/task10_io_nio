package com.epam.clientserver;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class MessageParser {
    private String message;
    private String receiverLogin;
    private String senderLogin;

    private MessageParser(String input) {
        senderLogin = extractSenderLogin(input);
        String query = input.substring(input.indexOf(senderLogin) + senderLogin.length() + 2);
        message = extractMessage(query);
        receiverLogin = extractReceiverLogin(query);
    }

    String getMessage() {
        return message;
    }

    String getReceiverLogin() {
        return receiverLogin;
    }

    String getSenderLogin() {
        return senderLogin;
    }

    static MessageParser parse(String input) {
        return new MessageParser(input);
    }

    private String extractReceiverLogin(String input) {
        Pattern p = Pattern.compile("^@(\\w+),\\s?.*$");
        Matcher m = p.matcher(input);
        if (m.find()) {
            return m.group(1);
        } else {
            return "";
        }
    }

    private String extractMessage(String input) {
        Pattern p = Pattern.compile("^@(\\w+),\\s?(.*)$");
        Matcher m = p.matcher(input);
        if (m.find()) {
            return m.group(2);
        } else {
            return input;
        }
    }

    private String extractSenderLogin(String input) {
        int endIndex = input.indexOf(":");
        return endIndex > 0 ? input.substring(0, input.indexOf(":")) : "";
    }
}
