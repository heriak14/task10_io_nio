package com.epam.clientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class Client {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final int BUFFER_SIZE = 1024;
    private static final int PORT = 9999;
    private SocketChannel server;
    private String login;
    private boolean isClose;

    private Client() {
        try {
            connectToServer();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void connectToServer() throws IOException {
        LOGGER.info("Enter your login: ");
        login = SCANNER.nextLine();
        InetSocketAddress address = new InetSocketAddress(InetAddress.getLocalHost(), PORT);
        server = SocketChannel.open(address);
        server.write(ByteBuffer.wrap(login.getBytes()));
    }

    private void showAllClients() throws IOException {
        ByteBuffer clients = ByteBuffer.allocate(BUFFER_SIZE);
        server.read(clients);
        LOGGER.info(new String(clients.array()));
    }

    private void sendMessage() throws IOException {
        String message;
        do {
            //showAllClients();
            LOGGER.info("Enter your message: ");
            message = SCANNER.nextLine();
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
            buffer.put((login + ": " + message).getBytes());
            buffer.flip();
            server.write(buffer);
        } while (!message.matches(".*exit"));
        isClose = true;
    }

    private void receiveMessage() throws IOException {
        while (!isClose) {
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
            server.read(buffer);
            String message = new String(buffer.array());
            LOGGER.info("\n" + new String(buffer.array()) + "\n");
        }
        server.close();
    }

    public void start() {
        Thread receive = new Thread(() -> {
            try {
                receiveMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Thread send = new Thread(() -> {
            try {
                sendMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        receive.start();
        send.start();
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }
}