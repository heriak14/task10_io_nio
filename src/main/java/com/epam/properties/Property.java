package com.epam.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public enum Property {
    DROID("droid.properties"), PATH("path.properties");

    private final Logger LOG = LogManager.getLogger();
    private final Properties properties;

    Property(String fileName) {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(fileName));
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }
}
