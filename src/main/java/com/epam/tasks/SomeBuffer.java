package com.epam.tasks;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.*;
import java.nio.channels.FileChannel;
import java.util.Objects;

public class SomeBuffer {
    private static SomeBuffer someBuffer;
    private FileChannel channel;
    private ByteBuffer buffer;

    private SomeBuffer(){
    }

    public static SomeBuffer allocate(int size) {
        if (Objects.isNull(someBuffer)) {
            someBuffer = new SomeBuffer();
            someBuffer.buffer = ByteBuffer.allocate(size);
        }
        return someBuffer;
    }

    public int read(RandomAccessFile file) throws IOException {
        channel = file.getChannel();
        return channel.read(buffer);
    }

    public int write(RandomAccessFile file) throws IOException {
        channel = file.getChannel();
        buffer.flip();
        return channel.write(buffer);
    }

    public byte[] getBytes() {
        return buffer.array();
    }

    public void setBytes(byte[] array) {
        buffer.put(array);
    }

}
