package com.epam.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;

public class PushBackReader extends InputStream {

    private InputStream inputStream;
    private Stack<Integer> buffer;

    public PushBackReader(InputStream is) {
        inputStream = is;
        buffer = new Stack<>();
    }

    @Override
    public int read() throws IOException {
        if (!buffer.empty()) {
            return buffer.pop();
        }
        return inputStream.read();
    }

    public void unread(int b) {
        buffer.push(b);
    }
}
