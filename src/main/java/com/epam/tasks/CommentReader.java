package com.epam.tasks;

import com.epam.properties.Property;

import java.io.*;
import java.util.Objects;

public class CommentReader implements AutoCloseable{
    private static final String FILE_PATH = Property.PATH.getProperty("file.string.path");
    private BufferedReader reader;

    public CommentReader() throws FileNotFoundException {
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(FILE_PATH)));
    }

    public String getComments() throws IOException {
        StringBuilder comments = new StringBuilder();
        boolean block = false;
        String line;
        while (Objects.nonNull(line = reader.readLine())) {
            if (line.contains("/*") || line.contains("*/")) {
                block = getBlockComment(line, comments);
            }
            if (block) {
                comments.append(line).append("\n");
                continue;
            }
            if (line.contains("//")) {
                comments.append(line.substring(line.indexOf("//"))).append("\n");
            }
        }
        return comments.toString();
    }

    private boolean getBlockComment(String line, StringBuilder store) {
        if (line.contains("/*")) {
            if (line.contains("*/")) {
                store.append(line, line.indexOf("/*"), line.indexOf("*/") + 2).append("\n");
                return false;
            } else {
                return true;
            }
        } else if (line.contains("*/")) {
            store.append(line, 0, line.indexOf("*/") + 2).append("\n");
            return false;
        }
        return false;
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }
}
