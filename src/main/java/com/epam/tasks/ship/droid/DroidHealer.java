package com.epam.tasks.ship.droid;


import com.epam.properties.Property;

public class DroidHealer extends Droid {
    private static final int HEALING_HEALTH = Integer.parseInt(Property
            .DROID.getProperty("droid.healer.health.healing"));
    private static final int REVIVING_HEALTH = Integer.parseInt(Property
            .DROID.getProperty("droid.healer.health.reviving"));

    public DroidHealer(int health, int power) {
        super(health, power);
    }

    public DroidHealer() {
        super();
    }

    public void heal(Droid droid) {
        if (this.getHealth() < HEALING_HEALTH) {
            return;
        }
        droid.setHealth(droid.getHealth() + HEALING_HEALTH);
        this.setHealth(getHealth() - HEALING_HEALTH);
        if (this.getHealth() <= 0) {
            this.setDead(true);
        }
    }

    public void revive(Droid droid) {
        if (this.getHealth() < REVIVING_HEALTH) {
            return;
        }
        if (droid.isDead()) {
            droid.setHealth(REVIVING_HEALTH);
            droid.setDead(false);
        }
    }
}
