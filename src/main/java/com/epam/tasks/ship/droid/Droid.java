package com.epam.tasks.ship.droid;

import com.epam.properties.Property;

import java.io.Serializable;
import java.util.Random;

public class Droid implements Serializable {

    private static final int MIN_HEALTH = Integer.parseInt(Property.DROID.getProperty("droid.health.min"));
    private static final int MAX_HEALTH = Integer.parseInt(Property.DROID.getProperty("droid.health.max"));
    private static final int MIN_POWER = Integer.parseInt(Property.DROID.getProperty("droid.power.min"));
    private static final int MAX_POWER = Integer.parseInt(Property.DROID.getProperty("droid.power.max"));
    private static final transient Random RANDOM = new Random();
    private int health;
    private int power;
    private transient boolean dead;

    Droid(int health, int power) {
        this.health = health;
        this.power = power;
    }

    public Droid() {
        this.health = RANDOM.nextInt(MAX_HEALTH - MIN_HEALTH + 1) + MIN_HEALTH;
        this.power = RANDOM.nextInt(MAX_POWER - MIN_POWER + 1) + MIN_POWER;
    }


    public int getHealth() {
        return health;
    }

    void setHealth(int health) {
        this.health = health;
    }

    boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public void attack(Droid droid) {
        droid.setHealth(droid.getHealth() - this.power);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Droid)) {
            return false;
        }
        return ((this.health == ((Droid) obj).health) && (this.power == ((Droid) obj).power));
    }

    @Override
    public int hashCode() {
        return health * power;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": health - " + health
                + ", power - " + power + ", " + (isDead() ? "dead" : "alive");
    }
}
