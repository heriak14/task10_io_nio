package com.epam.tasks.ship.container;

import com.epam.tasks.ship.droid.Droid;

import java.io.Serializable;
import java.util.AbstractQueue;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MyPriorityQueue<E extends Droid> extends AbstractQueue<E> implements Serializable {

    private class QueueNode<E extends Droid> {
        private E droid;
        private QueueNode<E> next;

        private QueueNode(E droid) {
            this.droid = droid;
        }
    }

    private QueueNode<E> head;
    private Comparator<E> comparator;
    private int size;

    public MyPriorityQueue() {
        comparator = (Comparator & Serializable)Comparator.comparingInt(Droid::getHealth);
    }

    public MyPriorityQueue(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private QueueNode<E> node = head;

            @Override
            public boolean hasNext() {
                return !Objects.isNull(node);
            }

            @Override
            public E next() {
                QueueNode<E> tmp = node;
                node = node.next;
                return tmp.droid;
            }
        };
    }

    @Override
    public void forEach(Consumer<? super E> action) {
        for (E droid : this) {
            action.accept(droid);
        }
    }

    @Override
    public boolean removeIf(Predicate<? super E> filter) {
        if (Objects.isNull(head)) {
            return false;
        }
        QueueNode<E> it = head;
        boolean removed = false;
        while (!Objects.isNull(it.next)) {
            if (filter.test(it.next.droid)) {
                it.next = it.next.next;
                removed = true;
                size--;
            } else {
                it = it.next;
            }
        }
        if (filter.test(head.droid)) {
            head = head.next;
            size--;
            return true;
        }
        return removed;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean offer(E e) {
        if (Objects.isNull(e)) {
            return false;
        }
        if (Objects.isNull(head)) {
            head = new QueueNode<>(e);
            size++;
            return true;
        }
        QueueNode<E> newNode = new QueueNode<>(e);
        if (comparator.compare(e, head.droid) < 0) {
            newNode.next = head;
            head = newNode;
            size++;
            return true;
        }
        QueueNode<E> it = head;
        while (!Objects.isNull(it.next)) {
            if (comparator.compare(e, it.next.droid) < 1) {
                newNode.next = it.next;
                it.next = newNode;
                size++;
                return true;
            }
            it = it.next;
        }
        it.next = newNode;
        size++;
        return true;
    }

    @Override
    public E poll() {
        if (Objects.isNull(head)) {
            return null;
        }
        QueueNode<E> tmp = head;
        head = head.next;
        size--;
        return tmp.droid;
    }

    @Override
    public E peek() {
        return Objects.isNull(head) ? null : head.droid;
    }
}
