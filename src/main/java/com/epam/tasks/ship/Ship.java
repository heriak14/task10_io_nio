package com.epam.tasks.ship;

import com.epam.tasks.ship.container.MyPriorityQueue;
import com.epam.tasks.ship.droid.Droid;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;

public class Ship<T extends Droid> {

    private Queue<T> loads;

    public Ship() {
        loads = new MyPriorityQueue<>();
    }

    public void addCargo(T cargo) {
        loads.add(cargo);
    }

    private void addLoads(List<? extends T> loads) {
        this.loads.addAll(loads);
    }

    public Queue<T> getLoads() {
        return loads;
    }

    public List<T> getLoadsIf(Predicate<? super T> p) {
        List<T> retLoads = new ArrayList<>(loads.size());
        for (T cargo : loads) {
            if (p.test(cargo)) {
                retLoads.add(cargo);
            }
        }
        return retLoads;
    }

    public void copyLoads(List<? super T> dest) {
        dest.addAll(loads);
    }

    public void serializeDroids(String fileName) throws IOException {
        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(fileName));
        List<T> list = new LinkedList<>(loads);
        stream.writeObject(list);
        stream.close();
    }

    public void deserializeDroids(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream stream = new ObjectInputStream(new FileInputStream(fileName));
        loads.clear();
        this.addLoads((LinkedList<T>) stream.readObject());
        stream.close();
    }
}
