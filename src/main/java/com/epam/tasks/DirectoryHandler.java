package com.epam.tasks;

import com.epam.properties.Property;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class DirectoryHandler {
    private static final String DIRECTORY_PATH = Property.PATH.getProperty("directory.traverse.path");
    private File file;

    public DirectoryHandler() {
        file = new File(DIRECTORY_PATH);
    }

    public String getDirectoryTree() {
        StringBuilder tree = new StringBuilder();
        traverse(file, "", tree);
        return tree.toString();
    }

    public String getFiles() {
        StringBuilder files = new StringBuilder();
        Optional.ofNullable(file.listFiles())
                .ifPresent(f -> Arrays.stream(f)
                        .forEach(file -> files.append(file.getName()).append("\n")));
        return files.toString();
    }

    public boolean changeDirectory(String directory) {
        if (directory.equals("..") && Objects.nonNull(file.getParentFile())) {
            file = file.getParentFile();
            return true;
        }
        File child = new File(file.getAbsolutePath() + "\\" + directory);
        if (child.exists()) {
            file = child;
            return true;
        }
        return false;
    }

    private void traverse(File file, String delimiter, StringBuilder tree) {
        for (File f : Objects.requireNonNull(file.listFiles())) {
            if (f.isFile()) {
                tree.append(delimiter).append(f.getName()).append("\n");
            } else {
                tree.append(delimiter).append(f.getName()).append("\n");
                traverse(f, delimiter + "\t", tree);
            }
        }
    }
}
