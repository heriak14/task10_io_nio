package com.epam.tasks;

import com.epam.properties.Property;

import java.io.*;

public class ReaderComparator implements AutoCloseable{

    private static final String FILE_PATH = Property.PATH.getProperty("file.comparator.path");
    private FileInputStream inputStream;

    public ReaderComparator() throws FileNotFoundException {
        inputStream = new FileInputStream(FILE_PATH);
    }

    public long getBufferedReaderTime(int size) throws IOException {
        return getReadTime(new BufferedInputStream(inputStream, size));
    }

    public long getFileReaderTime() throws IOException {
        return getReadTime(inputStream);
    }

    private long getReadTime(InputStream is) throws IOException {
        int b;
        long startTime = System.nanoTime();
        do {
            b = is.read();
        } while (b != -1);
        long endTime = System.nanoTime();
        is.close();
        return endTime - startTime;
    }

    @Override
    public void close() throws Exception {
        inputStream.close();
    }
}
