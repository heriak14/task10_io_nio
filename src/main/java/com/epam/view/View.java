package com.epam.view;

import com.epam.properties.Property;
import com.epam.tasks.*;
import com.epam.tasks.ship.Ship;
import com.epam.tasks.ship.droid.Droid;
import com.epam.tasks.ship.droid.DroidHealer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Serialize/deserialize ship with droids");
        menu.put("2", "Compare common reader with buffered reader");
        menu.put("3", "Test custom PushBackReader");
        menu.put("4", "Read comments from .java file");
        menu.put("5", "Show directory tree");
        menu.put("6", "Test custom NIO buffer");
        menu.put("Q", "Quit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testShipSerialization);
        methodsMenu.put("2", this::compareReadingTypes);
        methodsMenu.put("3", this::testPushBackReader);
        methodsMenu.put("4", this::readCommentsFromSourceFile);
        methodsMenu.put("5", this::showDirectoryTree);
        methodsMenu.put("6", this::testCustomBuffer);
        methodsMenu.put("q", this::quit);
    }

    private void showMenu(Map<String, String> menu) {
        LOGGER.trace("------------MENU------------\n");
        menu.forEach((k, v) -> LOGGER.info(k + " - " + v + "\n"));
    }

    private void testShipSerialization() {
        Ship<Droid> ship = new Ship<>();
        ship.addCargo(new Droid());
        ship.addCargo(new Droid());
        ship.addCargo(new DroidHealer());
        ship.getLoads().peek().setDead(false);
        try {
            ship.serializeDroids(Property.PATH.getProperty("ship.object.path"));
            ship.deserializeDroids(Property.PATH.getProperty("ship.object.path"));
            ship.getLoads().forEach(l -> LOGGER.info(l + "\n"));
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void compareReadingTypes() {
        try {
            LOGGER.info("Enter size of buffer: ");
            int bufferSize = SCANNER.nextInt();
            ReaderComparator comparator = new ReaderComparator();
            LOGGER.info("Usual reader time: " + comparator.getFileReaderTime());
            LOGGER.info("Buffered reader time: " + comparator.getBufferedReaderTime(bufferSize));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void testPushBackReader() {
        try (PushBackReader reader = new PushBackReader(
                new FileInputStream(Property.PATH.getProperty("file.text.path")))) {
            LOGGER.info("Enter number of bytes you want to read: ");
            int size = Math.min(SCANNER.nextInt(), reader.available());
            byte[] buffer = new byte[size];
            int data = reader.read();
            int i = 0;
            while (data != -1 && i < size) {
                buffer[i++] = (byte) data;
                data = reader.read();
            }
            LOGGER.info("Read data: " + new String(buffer) + "\n");
            LOGGER.info("Enter number of bytes you want to unread: ");
            size = Math.min(SCANNER.nextInt(), size);
            reader.unread(size);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void readCommentsFromSourceFile() {
        try (CommentReader reader = new CommentReader()) {
            LOGGER.info(reader.getComments());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void showDirectoryTree() {
        DirectoryHandler handler = new DirectoryHandler();
        LOGGER.info(handler.getDirectoryTree());
    }

    private void testCustomBuffer() {
        try (RandomAccessFile file = new RandomAccessFile(Property.PATH.getProperty("file.text.path"), "rw");) {
            SomeBuffer buffer = SomeBuffer.allocate((int) file.length());
            buffer.read(file);
            LOGGER.info("Read data:\n" + new String(buffer.getBytes()));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void quit() {
    }

    public void show() {
        String key;
        do {
            showMenu(menu);
            LOGGER.trace("Enter your choice: ");
            key = SCANNER.nextLine().toLowerCase();
            if (methodsMenu.containsKey(key)) {
                methodsMenu.get(key).print();
            } else if (!key.equals("q")) {
                LOGGER.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }
}
